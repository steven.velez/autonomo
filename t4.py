# ejercicio  4: Asume que ejecutamos las siguientes sentencias de asignación:


ancho = 17
alto = 12.0
#Para cada una de las expresiones siguientes, escribe el valor de la expresión y el tipo (del valor de la expresión).
n1 = ancho/2
n2 = ancho/2.0
n3 = alto/3
n4 = 1 + 2 * 5
print (n1)
print (type(n1))
print (n2)
print (type(n2))
print (n3)
print (type(n3))
print (n4)
print (type(n4))